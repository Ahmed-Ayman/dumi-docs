# The Code Generator User Manual

## The Editor  
Once you open the project, the first thing that you will be seeing is the 
whole editor with it's 3 panels like in the picture below.
![The Editor](../imgs/editor.png)

The Editor consists of 4 sections. 
- the top header
- the left panel
- the canvas
- the right panel

we will talk about each one in detail.
### Top Header:
![the top header](../imgs/top-header.png)

as you can see in the image above, the top header contains 6 sections, the first one is the project's logo plus another 
5 section that is providing some utilities.
- #### the panels toggling.

![the toggling ](../imgs/toggle.png)

this utility is responsible for toggling the panels in the editor. you can hide the pages section from the first button.
or hide the left panel completely from the 2nd button, or hide the right panel from the the 3rd button.
here are four pictures of the editor in each state.

  1. the normal state
  ![the editor](../imgs/editor.png)
  2. toggle the pages section
  ![](../imgs/pages-section.png)
  3. toggle the left panel
  ![](../imgs/toggle-left.png)
  4. toggle the right panel
  ![](../imgs/toggle-right.png)

- #### undo/redo.
- #### view modes.
- #### files manager
- #### preview modes. 

## User Manual
### - structure components

## Guidelines and best practices

For a better accessibility results, you have to use