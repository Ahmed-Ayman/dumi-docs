# The Code Generator
## Overview

The code generator is a software that makes the process of creating an awesome website a lot easier!
it's just a couple of drag and drop operations and then you get your webpage available to be used!

Our code generator is different than any available code generator since we've been focusing on making it follow 
the standards out of the box. like you will find it following most of the accessiblity guidelines provided 
by the W3C and the [WCAG](https://www.w3.org/WAI/standards-guidelines/wcag/).

We've been trying also to provide the most awesome user experience. 
at first we did our best to make the code generated so responsive. which means that it works very well on all
the devices. also you will find a lot of templates, widgets and ready
layouts that should make the user experience so much better! those additions were based upon our research for 
the Dumi Design System @TODO: add a link to the design system.

So, You will not just be creating  a beautiful website easily. but it will be a one that is favorable for both 
your browser and your users.

## Quick start
To use the software locally, you will need to have a web server that supports PHP.
you can use one like apache, nginx or even more simply just the built-in php server.

Here's the steps to get the php built-in server on both Linux/Windows.
@TODO : get the installation instruction
### PHP-built In server

### Run the project

to run the project simply move to the root directory of the project that is `dumi-code-gnerator` then type 
`php -S 127.0.0.1:8000` which will start the server to listen on port 8000. if you now navigate to localhost:8000 or 127.0.0.1:8000
you should find the project there up and running.

### Getting Started:
once the project is up and running, you should see the editr page as in the following image.

now you can start dragging and dropping elements to create your beautiful webpages.

![The editr](../imgs/editor.png)

> you will find a couple of examples that you can start tweaking to build your very
own webpage.
## User Manual
This docs include a separate section for the user manual over [here](/user-manual/user-manual).
> the user manual is pretty detailed and covers some cool tips and tricks that will keep you aligned 
with our design system.

## Guidelines and best practices

There's also a separate section for some guidelines for a better webpages design based on our research
for the Dumi design system.

It covers things like:
- how to follow the accessibility standards and best practices.
- how to follow the responsive design and the user experience best practices.
- and finally it includes some tips and tricks to make a better use of our code generator.